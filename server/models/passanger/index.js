module.exports = function(connection, Sequelize){
    
        var Passanger = connection.define('passanger', {
            pgrID: {
                type: Sequelize.INTEGER(11),
                allowNull: false,
                primaryKey: true,
                autoIncrement: true
            },

            pgrName: {
                type: Sequelize.STRING,
                allowNull: false
            },
            address:{
                type: Sequelize.STRING,
                allowNull: false
            }
        }, {
            freezeTableName: true, 
            timestamps: false
        });
        return Passanger;
    }