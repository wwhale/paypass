module.exports = function(connection, Sequelize){
    
        var Userlogin = connection.define('userlogin', {
            loginID: {
                type: Sequelize.INTEGER(11),
                allowNull: false,
                primaryKey: true,
                autoIncrement: true
            },
            loginpgrID: {
                type: Sequelize.INTEGER(11),
                allowNull: false,
                references: {
                    model: 'passanger',
                    key: 'pgrID'
                }
            },
            contactNo: {
                type: Sequelize.STRING,
                allowNull: false
            },

            loginTime: {
                type: Sequelize.DATE,
                allowNull: false
            },
            logoutTime: {
                type: Sequelize.DATE,
                allowNull: true
            },
            currentSess: {
                type: Sequelize.ENUM('Y','N'),
                allowNull: false
            },
            
            
        }, {
            freezeTableName: true, 
            timestamps: false
        });
        return Userlogin;
    }