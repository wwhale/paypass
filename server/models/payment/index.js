module.exports = function(connection, Sequelize){
    
        var Payment = connection.define('payment', {
            payID: {
                type: Sequelize.INTEGER(5),
                allowNull: false,
                primaryKey: true,
                autoIncrement: true
            },
            payregID: {
                type: Sequelize.INTEGER(11),
                allowNull: false,
                references: {
                    model: 'registartion',
                    key: 'regID'
                }
            },
            paypgrID: {
                type: Sequelize.INTEGER(11),
                allowNull: false,
                references: {
                    model: 'passanger',
                    key: 'pgrID'
                }
            },
            payDate: {
                type: Sequelize.DATE,
                allowNull: false,
            },
            payYear: {
                type: Sequelize.INTEGER(4),
                allowNull: false
            },
            payMth: {
                type: Sequelize.INTEGER(2),
                allowNull: false,
            },

            amount: {
                type: Sequelize.FLOAT,
                allowNull: false
            },

            payContact: {
                type: Sequelize.STRING,
                allowNull: false
            },
            payMode: {
                type: Sequelize.STRING,
                allowNull: false
            },

            remarks: {
                type: Sequelize.STRING,
                allowNull: true
            }
           
        }, {
            freezeTableName: true, 
            timestamps: false
        });
        return Payment;
    }