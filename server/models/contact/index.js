module.exports = function(connection, Sequelize){
    
        var Contact = connection.define('contact', {
            conID: {
                type: Sequelize.INTEGER(11),
                allowNull: false,
                primaryKey: true,
                autoIncrement: true
            },
            parID: {
                type: Sequelize.INTEGER(11),
                allowNull: false,
                references: {
                    model: 'parent',
                    key: 'parID'
                }
            },
            contactNo: {
                type: Sequelize.STRING,
                allowNull: false
            },
            contactType:{
                type: Sequelize.STRING,
                allowNull: false
            }
        }, {
            freezeTableName: true, 
            timestamps: false
        });
        return Contact;
    }