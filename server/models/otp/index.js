module.exports = function(connection, Sequelize){
    
        var OTP = connection.define('otp', {
             otpID: {
                type: Sequelize.INTEGER(11),
                allowNull: false,
                primaryKey: true,
                autoIncrement: true
            }, 
            contactNo:{
                type: Sequelize.STRING,
                allowNull: false
            },
            pgrID: {
                type: Sequelize.INTEGER(11),
                allowNull: false
            },
            otp: {
                type: Sequelize.INTEGER(6),
                allowNull: false
            },
            timeOtp:{
                type: Sequelize.DATE,
                allowNull: false
            },
            expire:{
                type: Sequelize.ENUM('Y','N'),
                allowNull: true
            }
        }, {
            freezeTableName: true, 
            timestamps: false,
           
           
        });
        return OTP;
    }