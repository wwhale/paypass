(function() {
    angular
    .module("PPPApp")
    .service("PPPAppAPI", [
        '$http', 
        '$state',
        PPPAppAPI
    ]);




    function PPPAppAPI($http,$state) { 
        var vm = this;

        
        vm.dbsTransac = function(){
            console.log("---------PPPAppAPI dbsTransac------")
            return $http.post("/dbsTransac");
        }


        vm.dbsToken = function(){
            console.log("---------PPPAppAPI dbsToken------")
           /*  
            return $http({
                method: 'POST'
                , url: '/dbsToken'
                
            });
 */
            return $http.post("/dbsToken");

        }


        vm.chkOtp = function (user){
            console.log("---------PPPAppAPI chkotp------")
            console.log("user > " + user.mobilen);
            console.log("user > " + user.otp);

           // user = JSON.stringify(user);
            console.log("user3 > " + user);
            //return $http.post("/api/chkotp/" + user)
            return $http({
                method: 'POST'
                , url: '/api/chkotp'
                , data: {mobilen: user.mobilen, otp: user.otp}
            });

        }

        vm.sendCode = function(contactno) { 
            console.log("---------PPPAppAPI sendCode------")

            //return $http.post("/api/sms") 
            //return $http({
            return $http({
                method: 'POST'
                , url: '/api/sms'
                , data: {contactno: contactno}
            });
        
        }

        vm.searchPassangerbyphone = function(contactno) { //create an end point in app.js
            console.log("---------PPPAppAPI searchPassengerbyPhone------")
            console.log("contactno > " + contactno);
            return $http.get("/api/chkcontact/" + contactno) //this is parameter (check app.js)
        }

        vm.getpgrProfilebycontact = function(contactno) { //create an end point in app.js
            console.log("---------PPPAppAPI getpgrProfilebycontact------")
            console.log("contactno > " + contactno);
            return $http.get("/api/passanger/" + contactno) //this is parameter (check app.js)
        }

        vm.getpgrPaymentsCurrentYear = function(pgrID) { //create an end point in app.js
            console.log("---------PPPAppAPI getpgrPaymentsCurrentYear------")
            console.log("pgrID > " + pgrID);
            return $http.get("/api/payments/" + pgrID) //this is parameter (check app.js)
        }

        vm.addPayRec = function(pgrID, amountToPay, month, mobile, regID) { //create an end point in app.js
            console.log("---------PPPAppAPI addPayRec------")
            console.log("pgrID > " + pgrID);
            console.log("amountToPay > " + amountToPay);
            console.log("pgrmonthID > " + month);

            // Setup the data
            data = {
                pgrID: pgrID,
                amountToPay: amountToPay,
                month: month,
                mobile: mobile,
                regID: regID
            };

            return $http.post("/api/addPayRec", data) //this is parameter (check app.js)
        }



   }

})();

/*


// redirectDbs no need to run from server side
  
        vm.redirectDbs = function(){

            return $http({
                method: 'GET'
                , url: '/redirectdbs'
                //, data: {mobilen: user.mobilen, otp: user.otp}
            });

        }
  


        function insertEmp(employee) {
            // This line returns the $http to the calling function
            // This configuration specifies that $http must send the employee data received from the calling function
            // to the /employees route using the HTTP POST method. $http returns a promise object. In this instance
            // the promise object is returned to the calling function
            return $http({
                method: 'POST'
                , url: 'api/employees'
                , data: {emp: employee}
            });
        }


        vm.searchPassangerbyphone = function(user) {

            console.log("---------PPPAppAPI searchPassengerbyPhone------")
            console.log("user > " + JSON.stringify(user));
            console.log("user.serviceprovider > " + user.serviceprovider);
            console.log("user.mobilen > " + user.mobilen);
            console.log("------return user after $http---------")
            return $http.get("/api/passanger",user);
            //return $http.get("/api/employees?keyword=" + keyword_value); //(3) because of $http it calls the get
        }                           //MULTIPLE ?keyword1=" + value1 + "&keyword2=" + value2



 function PPPAppAPI($http,$state) { 
        var vm = this;

        vm.searchPassangerbyphone = function(keyword_value, sortby, itemsPerPage, currentPage) {

            return $http.get(`/api/grocery?keyword=${keyword_value}&sortby=${sortby}&itemsPerPage=${itemsPerPage}&currentPage=${currentPage}`);
            //return $http.get("/api/employees?keyword=" + keyword_value); //(3) because of $http it calls the get
        }                           //MULTIPLE ?keyword1=" + value1 + "&keyword2=" + value2


        self.getGrocery = function(id) { //create an end point in app.js
            console.log(id);
            return $http.get("/api/grocery/" + id) //this is parameter (check app.js)
        }

        self.updateGrocery = function(grocery) { 
            console.log(grocery);
            return $http.put("/api/grocery", grocery);
        }

       self.addGrocery = function(grocery) {
            return $http.post("/api/grocery", grocery);
        }

//        self.deleteGrocery = function(emp_no) {
//            console.log(id);                             //PARAMETERIZE means xx/xx/ 
//            return $http.delete("/api/grocery/" + emp_no); //parameterize it on the URL 
//        }

*/