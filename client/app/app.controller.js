(function () {
    angular
        .module("PPPApp")
        .controller("PPPController", PPPController)
        .controller("PPPProfileCtrl", PPPProfileCtrl);


        PPPController.$inject = ['PPPAppAPI','$uibModal', '$state', '$rootScope', '$scope', '$localStorage','$sessionStorage'];
        PPPProfileCtrl.$inject = ['$state', '$rootScope', '$scope'];

    

    function  PPPProfileCtrl($state, $rootScope, $scope){
        var vm = this;

    } //end of PPPProfileCtrl

    
    function PPPController(PPPAppAPI, $uibModal, $state, $rootScope, $scope, $localStorage, $sessionStorage) {
       
        var vm = this;
        vm.format = "EEEE, MMMM d, y h:mm:ss a";

        vm.regcompanydefault = "JK59 Transport Pte Ltd";
        vm.regcompanynumdefault = 64655959;
        //objmobilen = 97229759;
        
      
        vm.noedit = 1;
        vm.pwcheck = 0; 
        vm.chkOK = 200;

        vm.appLockpw = '111';
        vm.otp = 111111;

/*         vm.appLockpw = '111';
        vm.pwcheck = 0; */
        
        //vm.user = {};
        //vm.user.show = 0;
       // vm.pgrrecord = {};

       /* $rootScope.$broadcast('status_updated', $sessionStorage.pgrrecord);
       
       $rootScope.$on('status_updated', function(event, obj){
           console.log(obj); // 10
           vm.pgrrecord = $sessionStorage.pgrrecord;
           console.log("sessionstorage.pgrrecord.contactNo > " +  vm.pgrrecord.contactNo);   
           console.log("sessionstorage.pgrrecord.contactNo > " +  vm.pgrrecord.parent.passanger.pgrName);  
           
       });  */
       vm.user = $sessionStorage.user;
       vm.pgrrecord = $sessionStorage.pgrrecord;
       vm.payments = $sessionStorage.payments;
      // console.log("sessionstorage.pgrrecord.contactNo > " +  vm.pgrrecord.contactNo);   

       //console.log("result.conID > " + vm.pgrrecord.conID);

        vm.checkApppw = checkApppw;
        vm.chkcontact = chkcontact;
        vm.initApp = initApp;
        vm.registerApp = registerApp;
        vm.unlockApp = unlockApp;
        vm.searchPassanger = searchPassanger;
        vm.getPgrProfPayment = getPgrProfPayment;
        vm.registerPayment = registerPayment;
        vm.reloadProfileFromMobile = reloadProfileFromMobile;
        vm.makePayment = makePayment;
        vm.redirect = redirect;
        vm.redirectDbs = redirectDbs;
        vm.otpgen = otpgen;
        vm.dbsToken = dbsToken;
        vm.dbsPay = dbsPay;
        vm.logoff = logoff;

        vm.months = [ 'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
        vm.monthsShort = [ 'JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC'];
        vm.isMonthPaid = [ false, false, false, false, false, false, false, false, false, false, false, false ];
        onProfilePageLoad();

        function onProfilePageLoad(){
            console.log('reading vm payments');
            console.log(vm.payments);
            
            if(vm.payments != null){
                vm.payments.forEach(function(payment){
                    vm.isMonthPaid[payment.payMth] = true;
                });
            }            
            vm.currMonth = -1;
            vm.showPaymentSection = false;
            vm.PaymentMessageTemplate = 'Click To Proceed $AMT To make payment for the month of MON';
            vm.PaymentMessage = '';
        }


        function logoff() {            
            sessionStorage.clear();
            $localStorage.$reset();
            
            console.log('====logoff====');
            location.reload();
            window.location.href = '/#!/load';            
        }


        function otpgen (){

            PPPAppAPI.sendCode(vm.user.mobilen).then((result)=>{
                vm.chkOK=result.status;
                console.log("sendotp result > " + vm.chkOK);
            }).catch((err)=>{
                vm.chkOK=err.status;
                console.log("sendotp err > " +  vm.chkOK);
            })

        }

       
        function redirectDbs(){
            console.log("----start redirect -----");

            PPPAppAPI.redirectDbs().then((result)=>{
                console.log("----start redirect---" + result.status);
            
            })
        }

        function dbsPay(){
            console.log("----start dbsPay -----");

            PPPAppAPI.dbsTransac().then((result)=>{
                console.log("----start dbsPay---" + result.status);
            
            })
        }


        function dbsToken(){
            console.log("----start dbsToken -----");

            PPPAppAPI.dbsToken().then((result)=>{
                console.log("----start dbsToken---" + result.status);
            
            })
        }

        function registerPayment() {
            // Get month 
            month = vm.currMonth;

            // Get Passenger
            pgrID = vm.pgrrecord.parent.pgrID;

            // Get Amount
            amountToPay = vm.pgrrecord.parent.passanger.registration.fees;

            // Get Mobile
            mobile = vm.pgrrecord.contactNo;

            // Get Registration Id
            regID = vm.pgrrecord.parent.passanger.registration.regID;

            // Add Payment record to DB 
            PPPAppAPI.addPayRec(pgrID, amountToPay, month, mobile, regID).then((result)=>{

                // TODO : Check response
                console.log("----payment recorded---" + result.status);

                // Reload Profile
                reloadProfileFromMobile(vm.user.mobilen, onProfilePageLoad);

             }).catch((error)=>{
                console.log("----payment recorded fail---" + result.status);
             })
        }

        function makePayment(monthNum){          

            // TODO : REVIEW THIS
            console.log(vm.payments.length)
            console.log(monthNum)
            if(vm.isMonthPaid[monthNum]){
                vm.showPaymentSection = false;
                return;
            }

            vm.currMonth = monthNum;
            vm.showPaymentSection = true;
            vm.PaymentMessage = vm.PaymentMessageTemplate.toString().replace('AMT', vm.pgrrecord.parent.passanger.registration.fees).replace('MON', vm.months[monthNum]);
        }


        function redirect(){
            console.log("----start redirect -----");

            
            window.location.href = 'https://www.dbs.com/sandbox/api/sg/v1/oauth/authorize?client_id=ff8b5b2a-9b9f-4ee1-be9c-23b1e50c966e&redirect_uri=http%3A%2F%2Fjk59.com:888%2Fredirect&scope=Read&response_type=code&state=0399';
            
/* 
            var redirecturi = 'http://208daf7b.ngrok.io/redirect';
            var uri = encodeURIComponent(redirecturi);
            console.log("encodeuri > " + uri);
 */
           
            //window.location.href = 'https://www.dbs.com/sandbox/api/sg/v1/oauth/authorize?client_id=ff8b5b2a-9b9f-4ee1-be9c-23b1e50c966e&redirect_uri=http%3A%2F%2F118.189.176.177%2Fredirect&scope=Read&response_type=code&state=0399';
            
           //  window.location.href = 'https://www.dbs.com/sandbox/api/sg/v1/oauth/authorize?client_id=ff8b5b2a-9b9f-4ee1-be9c-23b1e50c966e&redirect_uri=http%3A%2F%2F7603319d.ngrok.io%2Fredirect&scope=Read&response_type=code&state=0399';

            //window.location.href = 'https://www.dbs.com/sandbox/api/sg/v1/oauth/authorize?client_id=64465ce4-6961-4156-b292-643bcf1bf8b0&redirect_uri=https%3A%2F%2Fwww.dbs.com%2Fdevelopers%2F%23%2Fall-products%2Fplay-ground&scope=Read&response_type=code&state=0399';
            
             console.log("hash > " + window.location.hash);
        }


    
        function chkcontact(){
           
            console.log("------load chkcontact-----");
            console.log(vm.user.mobilen);

            $sessionStorage.user = vm.user;
            console.log($sessionStorage);

            PPPAppAPI.searchPassangerbyphone(vm.user.mobilen).then((result)=>{
                console.log("----searchPassangerbyphone result---" + result.status);
                console.log("----searchPassangerbyphone data ---" + JSON.stringify(result.data));

                if (result.data != null){
                    vm.chkOK=result.status;
                    console.log("result > " + vm.chkOK);
                    console.log("send Otp to > " + vm.user.mobilen);

                    PPPAppAPI.sendCode(vm.user.mobilen).then((result)=>{
                        vm.chkOK=result.status;
                        console.log("sendotp result > " + vm.chkOK);
                    }).catch((err)=>{
                        vm.chkOK=err.status;
                        console.log("sendotp err > " +  vm.chkOK);
                    })    

                    $rootScope.backgroundImg = "url('/image/slider_image_2.jpeg')";
                    $state.go('registerotp');
                }else{
                    vm.chkOK = 201;
                    console.log("searchpassangerbyphone results > " +  vm.chkOK);
                }

                }).catch((err)=>{
                    vm.chkOK=err.status;
                    console.log("searchpassangerbyphone err > " +  vm.chkOK);
                })    
            }

        function checkApppw(){

            console.log("------load checkApppw-----");
  
            if (vm.user.pw == vm.appLockpw) {
               console.log("pw > " + vm.user.pw);
               console.log("appLockpw > " + vm.appLockpw);
               $rootScope.backgroundImg = "url('/image/white.jpeg')";
                $state.go('profile');
            } else {
            console.log("------password wrong-----");               
                vm.pwcheck = 1;
                

            }

        }

        function getPgrProfPayment(pgrID, callback){
            PPPAppAPI.getpgrPaymentsCurrentYear(pgrID).then((result)=> {
                console.log("----start getpgrPaymentsCurrentYear---");
                vm.payments = result.data;
                console.log(JSON.stringify(vm.payments));

                console.log("---------Save to session storage----");  
                $sessionStorage.payments = vm.payments;      
                
                vm.chkOK=result.status;
                callback(vm.chkOK);
            }).catch((error) => {                                
                vm.chkOK=error.status;
                callback(vm.chkOK);
            })
        }


        function searchPassanger(objmobilen, callback){

            console.log("----start searchPassanger---");
            
            PPPAppAPI.searchPassangerbyphone(objmobilen).then((result)=>{

                console.log("----found phone---" + result.status);
                console.log("----found phone---" + result.data);

                if (result.data != null){

                        PPPAppAPI.getpgrProfilebycontact(objmobilen).then((result)=>{
                        
                        console.log("----start getpgrProfilebycontact 7777---");

                        vm.pgrrecord = result.data;

                        console.log(JSON.stringify(vm.pgrrecord));
                        /* console.log("result.conID > " + vm.pgrrecord.parent.passanger.registrations.class.school);
                        console.log("result.conID > " + vm.pgrrecord.conID); */
                        
                        
                        console.log("---------Save to session storage----");  
                        console.log(vm.pgrrecord);
                        $sessionStorage.pgrrecord = vm.pgrrecord;

                        console.log("sessionstorage.... > " +  $sessionStorage.pgrrecord.contactNo);   
                        console.log("sessionstorag.... > " +  $sessionStorage.pgrrecord.parent.passanger.pgrName);  
                        console.log("sessionstorag.... > " +  $sessionStorage.pgrrecord.parent.passanger.registration.sess); 
                        console.log("sessionstorag.... > " +  $sessionStorage.pgrrecord.parent.passanger.registration.class.class);   
                        //console.log(JSON.stringify(vm.pgrrecord.parent));
                        //console.log(JSON.stringify(vm.pgrrecord.parent.passanger));

                        vm.chkOK=result.status;
                        /* console.log(">> status >" + result);
                        console.log(">> status >" + result.status); */
                        callback(vm.chkOK);
                        
                        // console.log("result.parID > " + pgrrecord.parID);
                        // console.log("result.contactNo > " + pgrrecord.contactNo);
                        // console.log("result.contactType > " + pgrrecord.contactType);

                    // console.log("---------$rootScope.$broadcast after API----");  

                // $rootScope.$broadcast('passanger_record', vm.pgrrecord);
                        //console.log(pgrrecord.parent.parID);
                        //console.log(pgrrecord.parent.pgrID);
                        //console.log(pgrrecord.parent.passanger.pgrID);       
                        //console.log(pgrrecord.parent.passanger.pgrName);

                    // console.log("result.passanger > " + JSON.parse(pgrrecord.parent)); */

                    }).catch((error)=>{
                        vm.chkOK=error.status;
                        /*  console.log(error);
                        console.log(">> status >" + error.status); */
                        callback(vm.chkOK)
                        })

                } else{
                    vm.chkOK = 201;
                    callback(vm.chkOK);

                }
               
               /*      console.log("----start searchPassangerbyphone1---");

                        vm.pgrrecord = result.data;

                        console.log("result.conID > " + vm.pgrrecord.conID);

                        console.log("---------Save to session storage----");  
                        $sessionStorage.pgrrecord = vm.pgrrecord;

                        console.log("sessionstorage.pgrrecord.contactNo > " +  $sessionStorage.pgrrecord.contactNo);   
                        console.log("sessionstorage.pgrrecord.contactNo > " +  $sessionStorage.pgrrecord.parent.passanger.pgrName);  
                        
                        vm.chkOK=result.status;
                        /* console.log(">> status >" + result);
                        console.log(">> status >" + result.status); 
                        callback(vm.chkOK);
                        
                        // console.log("result.parID > " + pgrrecord.parID);
                        // console.log("result.contactNo > " + pgrrecord.contactNo);
                        // console.log("result.contactType > " + pgrrecord.contactType);

                    // console.log("---------$rootScope.$broadcast after API----");  

                // $rootScope.$broadcast('passanger_record', vm.pgrrecord);
                        //console.log(pgrrecord.parent.parID);
                        //console.log(pgrrecord.parent.pgrID);
                        //console.log(pgrrecord.parent.passanger.pgrID);       
                        //console.log(pgrrecord.parent.passanger.pgrName);

                    // console.log("result.passanger > " + JSON.parse(pgrrecord.parent)); */

             }).catch((error)=>{
                vm.chkOK=error.status;
               /*  console.log(error);
                console.log(">> status >" + error.status); */
                callback(vm.chkOK)
             })

        

        }


        function initApp(){
            var vm = this;

            vm.pwcheck = 0;
 
            console.log("init >" + vm.pwcheck);
            // console.log("user.serviceprovider > " + vm.user.serviceprovider);
           // console.log("user.mobilen > " + vm.user.mobilen);

            vm.pwcheck ++;
            console.log("pwcheck >" + vm.pwcheck);


        }

       function reloadProfileFromMobile(mobile, callback){
            searchPassanger(mobile, function(status) {

                console.log("searchPassanger status > " + status);

                if(status == 200){
                    getPgrProfPayment(vm.pgrrecord.parent.pgrID,  function(status){
                        console.log(status);
                        if(status == 200){
                            $rootScope.backgroundImg = "url('/image/white.jpeg')";                                
                            $state.go('profile');
                            callback();
                        }
                    });                            
                }
            });
       }

        function registerApp(){
            console.log("------Start registerApp-----");
            console.log(vm.user);


            PPPAppAPI.chkOtp(vm.user). then((result)=>{

                // console.log("chkotp result >>" + JSON.stringify(result));
                console.log("chkotp result >>" + result.status); 
              

                if(result.status == 200){
                    vm.chkOK = 200;
                    console.log( vm.chkOK);

                    reloadProfileFromMobile(vm.user.mobilen, onProfilePageLoad);
                  
                }else if (result.status == 201) {
                    vm.chkOK = 201;
                    console.log( vm.chkOK);
                }else if (result.status == 202){
                    vm.chkOK = 202;
                }
            
            }).catch((error)=>{
                vm.chkOK=error.status;
            /*  console.log(error);
                console.log(">> status >" + error.status); */
            })

        }//end registerApp


       // vm.initApp();
    
    }// end of PPPController()


    function unlockApp(size,parentSelector){
        console.log("------Start registerApp-----");
        var parentElem = parentSelector ? 
        angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
        var modalInstance = $uibModal.open({
            animation: self.animationsEnabled,
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            templateUrl: './loginform.html',
            controller: 'PPPCloseModalController',
            controllerAs: 'ctrl',
            size: 'lg',
            appendTo: parentElem,
            resolve: {
                items: function () {
                    return size;
                }
            }
        }).result.catch(function (resp) {
            if (['cancel', 'backdrop click', 'escape key press'].indexOf(resp) === -1)      throw resp;
        });
    }


    
})();
