(function () {
    angular
        .module("PPPApp")
        .controller("PPPController", PPPController)
        .controller("PPPCloseModalController", PPPCloseModalController)
        .controller("PPPProfileCtrl", PPPProfileCtrl);


        PPPController.$inject = ['PPPAppAPI','$uibModal', '$state', '$rootScope', '$scope', '$localStorage','$sessionStorage'];
        PPPCloseModalController.$inject = ['$uibModalInstance','$state', '$rootScope', '$scope'];
        PPPProfileCtrl.$inject = ['$state', '$rootScope', '$scope'];



    function  PPPProfileCtrl($state, $rootScope, $scope){
        var vm = this;

    } //end of PPPProfileCtrl

    
    function PPPController(PPPAppAPI, $uibModal, $state, $rootScope, $scope, $localStorage, $sessionStorage) {
       

       
        
        var vm = this;
        vm.format = "EEEE, MMMM d, y h:mm:ss a";

        vm.regcompanydefault = "Best Company";
        vm.regcompanynumdefault = "97229759";
      
        vm.noedit = 1;
        vm.pwcheck = 0; 
        vm.chkOK = 200;

        vm.appLockpw = '111';
        vm.otp = 111111;

/*         vm.appLockpw = '111';
        vm.pwcheck = 0; */
        
        //vm.user = {};
        //vm.user.show = 0;
       // vm.pgrrecord = {};

       /* $rootScope.$broadcast('status_updated', $sessionStorage.pgrrecord);
       
       $rootScope.$on('status_updated', function(event, obj){
           console.log(obj); // 10
           vm.pgrrecord = $sessionStorage.pgrrecord;
           console.log("sessionstorage.pgrrecord.contactNo > " +  vm.pgrrecord.contactNo);   
           console.log("sessionstorage.pgrrecord.contactNo > " +  vm.pgrrecord.parent.passanger.pgrName);  
           
       });  */
       vm.user = $sessionStorage.user;
       vm.pgrrecord = $sessionStorage.pgrrecord;
      // console.log("sessionstorage.pgrrecord.contactNo > " +  vm.pgrrecord.contactNo);   

       //console.log("result.conID > " + vm.pgrrecord.conID);

        vm.checkApppw = checkApppw;
        vm.chkcontact = chkcontact;
        vm.initApp = initApp;
        vm.registerApp = registerApp;
        vm.unlockApp = unlockApp;
        vm.searchPassanger = searchPassanger;
        vm.redirect = redirect;



        function redirect(){
            console.log("----start redirect -----");

            //window.location.href = "https://api.ocbc.com:8243/transactional/payanyone/1.0"
            objmobilen = 97229759;
            PPPAppAPI.sendCode(objmobilen).then((result)=>{
                console.log("----XXXXXXX---" + result.status);


               /*  if (result.data != null){
                    vm.chkOK=result.status;
                    console.log("result > " + vm.chkOK);
                    //$state.go('registerotp');
                }else{
                    vm.chkOK = 201;
                    console.log( vm.chkOK);
                }

                }).catch((error)=>{
                    vm.chkOK=error.status;
                })     */
            }

           // window.location.href = 'https://api.ocbc.com/ocbcauthentication/api/oauth2/authorize?client_id=zabLIis2DiZ_YWOPuHXfVyju5b0a&redirect_uri=http://localhost:3000/#!/profile>';
       
            /* to set http header
            
                $http.get('www.google.com/someapi', {
                headers: {'Authorization': 'Basic QWxhZGRpbjpvcGVuIHNlc2FtZQ=='}
            }); */
       
        }
    
        function chkcontact(objmobilen){
           
            console.log("------load chkcontact-----");
            console.log(objmobilen);
            console.log(vm.user.mobilen);

            $sessionStorage.user = vm.user;
            console.log($sessionStorage);

            PPPAppAPI.searchPassangerbyphone(objmobilen).then((result)=>{
                console.log("----found phone---" + result.status);
                console.log("----found phone---" + JSON.stringify(result.data));

                if (result.data != null){
                    vm.chkOK=result.status;
                    console.log("result > " + vm.chkOK);
                    $state.go('registerotp');
                }else{
                    vm.chkOK = 201;
                    console.log( vm.chkOK);
                }

                }).catch((error)=>{
                    vm.chkOK=error.status;
                })    
            }

        function checkApppw(){

            console.log("------load checkApppw-----");
  
            if (vm.user.pw == vm.appLockpw) {
               console.log("pw > " + vm.user.pw);
               console.log("appLockpw > " + vm.appLockpw);
                
                $state.go('profile');
            } else {
            console.log("------password wrong-----");               
                vm.pwcheck = 1;
                

            }

        }


        function searchPassanger(objmobilen, callback){

            console.log("----start searchPassanger---");
            
            PPPAppAPI.searchPassangerbyphone(objmobilen).then((result)=>{

                console.log("----found phone---" + result.status);
                console.log("----found phone---" + result.data);

                if (result.data != null){

                        PPPAppAPI.getpgrProfilebycontact(objmobilen).then((result)=>{
                        
                        console.log("----start getpgrProfilebycontact 7777---");

                        vm.pgrrecord = result.data;

                        console.log(JSON.stringify(vm.pgrrecord));
                        /* console.log("result.conID > " + vm.pgrrecord.parent.passanger.registrations.class.school);
                        console.log("result.conID > " + vm.pgrrecord.conID); */
                        

                        console.log("---------Save to session storage----");  
                        $sessionStorage.pgrrecord = vm.pgrrecord;

                        console.log("sessionstorage.... > " +  $sessionStorage.pgrrecord.contactNo);   
                        console.log("sessionstorag.... > " +  $sessionStorage.pgrrecord.parent.passanger.pgrName);  
                        console.log("sessionstorag.... > " +  $sessionStorage.pgrrecord.parent.passanger.registration.sess); 
                        console.log("sessionstorag.... > " +  $sessionStorage.pgrrecord.parent.passanger.registration.class.class);   
                        console.log(JSON.stringify(vm.pgrrecord.parent));
                        console.log(JSON.stringify(vm.pgrrecord.parent.passanger));

                        vm.chkOK=result.status;
                        /* console.log(">> status >" + result);
                        console.log(">> status >" + result.status); */
                        callback(vm.chkOK);
                        
                        // console.log("result.parID > " + pgrrecord.parID);
                        // console.log("result.contactNo > " + pgrrecord.contactNo);
                        // console.log("result.contactType > " + pgrrecord.contactType);

                    // console.log("---------$rootScope.$broadcast after API----");  

                // $rootScope.$broadcast('passanger_record', vm.pgrrecord);
                        //console.log(pgrrecord.parent.parID);
                        //console.log(pgrrecord.parent.pgrID);
                        //console.log(pgrrecord.parent.passanger.pgrID);       
                        //console.log(pgrrecord.parent.passanger.pgrName);

                    // console.log("result.passanger > " + JSON.parse(pgrrecord.parent)); */

                    }).catch((error)=>{
                        vm.chkOK=error.status;
                        /*  console.log(error);
                        console.log(">> status >" + error.status); */
                        callback(vm.chkOK)
                        })

                } else{
                    vm.chkOK = 201;
                    callback(vm.chkOK);

                }
               
               /*      console.log("----start searchPassangerbyphone1---");

                        vm.pgrrecord = result.data;

                        console.log("result.conID > " + vm.pgrrecord.conID);

                        console.log("---------Save to session storage----");  
                        $sessionStorage.pgrrecord = vm.pgrrecord;

                        console.log("sessionstorage.pgrrecord.contactNo > " +  $sessionStorage.pgrrecord.contactNo);   
                        console.log("sessionstorage.pgrrecord.contactNo > " +  $sessionStorage.pgrrecord.parent.passanger.pgrName);  
                        
                        vm.chkOK=result.status;
                        /* console.log(">> status >" + result);
                        console.log(">> status >" + result.status); 
                        callback(vm.chkOK);
                        
                        // console.log("result.parID > " + pgrrecord.parID);
                        // console.log("result.contactNo > " + pgrrecord.contactNo);
                        // console.log("result.contactType > " + pgrrecord.contactType);

                    // console.log("---------$rootScope.$broadcast after API----");  

                // $rootScope.$broadcast('passanger_record', vm.pgrrecord);
                        //console.log(pgrrecord.parent.parID);
                        //console.log(pgrrecord.parent.pgrID);
                        //console.log(pgrrecord.parent.passanger.pgrID);       
                        //console.log(pgrrecord.parent.passanger.pgrName);

                    // console.log("result.passanger > " + JSON.parse(pgrrecord.parent)); */

             }).catch((error)=>{
                vm.chkOK=error.status;
               /*  console.log(error);
                console.log(">> status >" + error.status); */
                callback(vm.chkOK)
             })

        

        }


        function initApp(){
            var vm = this;

            vm.pwcheck = 0;
 
            console.log("init >" + vm.pwcheck);
            // console.log("user.serviceprovider > " + vm.user.serviceprovider);
           // console.log("user.mobilen > " + vm.user.mobilen);

            vm.pwcheck ++;
            console.log("pwcheck >" + vm.pwcheck);


        }

       

        function unlockApp(size,parentSelector){
            console.log("------Start registerApp-----");
            var parentElem = parentSelector ? 
            angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
            var modalInstance = $uibModal.open({
                animation: self.animationsEnabled,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: './loginform.html',
                controller: 'PPPCloseModalController',
                controllerAs: 'ctrl',
                size: 'lg',
                appendTo: parentElem,
                resolve: {
                    items: function () {
                        return size;
                    }
                }
            }).result.catch(function (resp) {
                if (['cancel', 'backdrop click', 'escape key press'].indexOf(resp) === -1)      throw resp;
            });
        }

      
        function registerApp(size,parentSelector){
            console.log("------Start registerApp-----");
            console.log(vm.user.mobilen);

            if (vm.user.otp == vm.otp){

                searchPassanger(vm.user.mobilen, function(status) {
                    console.log("I GO THE STATUS >>");
                    console.log(status);

            /*  $scope.$on('passanger_record', function(event, obj){
                    console.log("---------$scope.$on----");   
                    console.log(obj.conID); // 10
                    vm.pgrrecord = obj;
                    console.log("obj.conID > " + pgrrecord.conID);
                }) */

                if(status == 200){
                    $state.go('profile');
                    /* var parentElem = parentSelector ? 
                    angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
                    var modalInstance = $uibModal.open({
                        animation: self.animationsEnabled,
                        ariaLabelledBy: 'modal-title',
                        ariaDescribedBy: 'modal-body',
                        templateUrl: './registerOK.html',
                        controller: 'PPPCloseModalController',
                        controllerAs: 'ctrl',
                        size: 'lg',
                        appendTo: parentElem,
                        resolve: {
                            items: function () {
                                return size;
                            }
                        }
                    }).result.catch(function (resp) {
                        if (['cancel', 'backdrop click', 'escape key press'].indexOf(resp) === -1)      throw resp;
                    }); */

                }else{
                    vm.chkOK = 201;
                    /* var parentElem = parentSelector ? 
                    angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
                    var modalInstance = $uibModal.open({
                        animation: self.animationsEnabled,
                        ariaLabelledBy: 'modal-title',
                        ariaDescribedBy: 'modal-body',
                        templateUrl: './registerOKfail.html',
                        controller: 'PPPCloseModalController',
                        controllerAs: 'ctrl',
                        size: 'lg',
                        appendTo: parentElem,
                        resolve: {
                            items: function () {
                                return size;
                            }
                        }
                    }).result.catch(function (resp) {
                        if (['cancel', 'backdrop click', 'escape key press'].indexOf(resp) === -1)      throw resp;
                    }); */

                }
                });
            }else{
                vm.chkOK = 201;
            }
        }//end registerApp


       // vm.initApp();
    
    }// end of PPPController()



 //        PPPCloseModalController is not longer using.
    function PPPCloseModalController($uibModalInstance, $state, $rootScope, $scope){
        var vm = this;
        vm.profileMain = profileMain;
        vm.loginFailed = loginFailed;

        function profileMain(){
            console.log("------load profileMain-----");
           //console.log("vm.user > " + vm.user.mobilen);
           // console.log("vm.pgrrecord > " + vm.pgrrecord.conID);
           // console.log("vm.pgrrecord > " + JSON.parse(pgrrecord.parent));

            $uibModalInstance.close(self.run);
           // console.log("---------$rootScope.$broadcast----");  
            //console.log("vm.pgrrecord > " + vm.pgrrecord.conID);
           // $rootScope.$broadcast('passanger_record', vm.pgrrecord);
            $state.go('profile',vm.pgrrecord);

        }
        

        function loginFailed(){
        console.log("------load loginFailed-----");
            $uibModalInstance.close(self.run);
            $state.go('load');
        }


    }   // end of PPPCloseModalController()



    
})();


/*

       function registerForm(){

            console.log("------load registerForm-----");

            $state.go('register');
        } 

var myObject = {
    status : 10
}

$rootScope.$broadcast('status_updated', myObject);

$rootScope.$on('status_updated', function(event, obj){
    console.log(obj.status); // 10
})




        .controller("EditEmployeeCtrl", EditEmployeeCtrl)
        .controller("AddEmployeeCtrl", AddEmployeeCtrl)
        .controller("DeleteEmployeeCtrl", DeleteEmployeeCtrl);

    EditEmployeeCtrl.$inject = ['$uibModalInstance', 'EMSAppAPI', 'items', '$rootScope', '$scope'];
    AddEmployeeCtrl.$inject = ['$uibModalInstance', 'EMSAppAPI', 'items', '$rootScope', '$scope'];
    DeleteEmployeeCtrl.$inject = ['$uibModalInstance', 'EMSAppAPI', 'items', '$rootScope', '$scope'];

 function EmployeeController(EMSAppAPI, $uibModal, $document, $scope) {
        var self = this;
        self.format = "M/d/yy h:mm:ss a";
        self.employees = [];
        self.maxsize=5;
        self.totalItems = 0;
        self.itemsPerPage = 10;
        self.currentPage = 1;

        self.searchEmployees =  searchEmployees;
        self.addEmployee =  addEmployee;
        self.editEmployee = editEmployee;
        self.deleteEmployee = deleteEmployee;
        self.pageChanged = pageChanged;

        function searchAllEmployees(searchKeyword,orderby,itemsPerPage,currentPage){
            EMSAppAPI.searchEmployees(searchKeyword, orderby, itemsPerPage, currentPage).then((results)=>{
                self.employees = results.data.rows;
                self.totalItems = results.data.count;
                $scope.numPages = Math.ceil(self.totalItems /self.itemsPerPage);
            }).catch((error)=>{
                console.log(error);
            });
        }


        function pageChanged(){
            console.log("Page changed " + self.currentPage);
            searchAllEmployees(self.searchKeyword, self.orderby,self.itemsPerPage, self.currentPage);
            console.log($scope.numPages);
        }

        $scope.$on("refreshEmployeeList",function(){
            console.log("refresh employee list "+ self.searchKeyword);
            searchAllEmployees(self.searchKeyword, self.orderby,self.itemsPerPage, self.currentPage);
        });

        $scope.$on("refreshEmployeeListFromAdd",function(event, args){
            console.log("refresh employee list from emp_no"+ args.emp_no);
            var employees = [];
            employees.push(args);
            self.searchKeyword = "";
            self.employees = employees;
        });

        function searchEmployees(){
            console.log("search employees  ....");
            console.log(self.orderby);
            searchAllEmployees(self.searchKeyword, self.orderby,self.itemsPerPage, self.currentPage);
        }

       

        

       
    }



    function DeleteEmployeeCtrl($uibModalInstance, EMSAppAPI, items, $rootScope, $scope){
        var self = this;
        //self.items = items;
        self.deleteEmployee = deleteEmployee;
        console.log(items);
        EMSAppAPI.getEmployee(items).then((result)=>{
            console.log(result.data);
            self.employee =  result.data;
            self.employee.birth_date = new Date( self.employee.birth_date);
            self.employee.hire_date = new Date( self.employee.hire_date);
            console.log(self.employee.birth_date);
        });

        function deleteEmployee(){
            console.log("delete employee ...");
            EMSAppAPI.deleteEmployee(self.employee.emp_no).then((result)=>{
                console.log(result);
                $rootScope.$broadcast('refreshEmployeeList');
                $uibModalInstance.close(self.run);
            }).catch((error)=>{
                console.log(error);
            });
        }

    }

    function AddEmployeeCtrl($uibModalInstance, EMSAppAPI, items, $rootScope, $scope){
        console.log("Add Employee");
        var self = this;
        self.saveEmployee = saveEmployee;

        self.employee = {
            gender: "M"
        }
        initializeCalendar($scope);
        function saveEmployee(){
            console.log("save employee ...");
            console.log(self.employee.first_name);
            console.log(self.employee.last_name);
            console.log(self.employee.hire_date);
            console.log(self.employee.birth_date);
            console.log(self.employee.gender);
            EMSAppAPI.addEmployee(self.employee).then((result)=>{
                //console.log(result);
                console.log("Add employee -> " + result.emp_no);
                $rootScope.$broadcast('refreshEmployeeListFromAdd', result.data);
             }).catch((error)=>{
                console.log(error);
                self.errorMessage = error;
             })
            $uibModalInstance.close(self.run);
        }
    }




function initializeCalendar($scope){
        self.datePattern = /^\d{4}-\d{2}-\d{2}$/;;
        
        $scope.today = function() {
            $scope.dt = new Date();
        };
        $scope.today();
    
        $scope.clear = function() {
            $scope.dt = null;
        };
    
        $scope.inlineOptions = {
            customClass: getDayClass,
            minDate: new Date(),
            showWeeks: true
        };
    
        $scope.dateOptions = {
            //dateDisabled: disabled,
            formatYear: 'yy',
            maxDate: new Date(2020, 5, 22),
            minDate: new Date(),
            startingDay: 1
        };

        // Disable weekend selection
        function disabled(data) {
            console.log(data);
            var date = data.date,
                mode = data.mode;
            return mode === 'day' && (date.getDay() === 0 || date.getDay() === 6);
        }
    
        $scope.toggleMin = function() {
        $scope.inlineOptions.minDate = $scope.inlineOptions.minDate ? null : new Date();
        $scope.dateOptions.minDate = $scope.inlineOptions.minDate;
        };
    
        $scope.toggleMin();
    
        $scope.open1 = function() {
            $scope.popup1.opened = true;
        };

        $scope.open2 = function() {
            $scope.popup2.opened = true;
        };
    
        $scope.setDate = function(year, month, day) {
            $scope.dt = new Date(year, month, day);
        };
    
        $scope.formats = ['dd-MMMM-yyyy', 'yyyy-MM-dd', 'dd.MM.yyyy', 'shortDate'];
        $scope.format = $scope.formats[1];
        $scope.altInputFormats = ['M!/d!/yyyy'];
    
        $scope.popup1 = {
            opened: false
        };

        $scope.popup2 = {
            opened: false
        };
    
        var tomorrow = new Date();
        tomorrow.setDate(tomorrow.getDate() + 1);
        var afterTomorrow = new Date();
        afterTomorrow.setDate(tomorrow.getDate() + 1);
        $scope.events = [
        {
            date: tomorrow,
            status: 'full'
        },
        {
            date: afterTomorrow,
            status: 'partially'
        }
        ];
    
        function getDayClass(data) {
            var date = data.date,
                mode = data.mode;
            if (mode === 'day') {
                var dayToCheck = new Date(date).setHours(0,0,0,0);
        
                for (var i = 0; i < $scope.events.length; i++) {
                var currentDay = new Date($scope.events[i].date).setHours(0,0,0,0);
        
                if (dayToCheck === currentDay) {
                    return $scope.events[i].status;
                }
                }
            }
        
            return '';
        }
    }





    function EditEmployeeCtrl($uibModalInstance, EMSAppAPI, items, $rootScope, $scope){
        console.log("Edit Employee Ctrl");
        var self = this;
        self.items = items;
        initializeCalendar($scope);

        EMSAppAPI.getEmployee(items).then((result)=>{
           console.log(result.data);
           self.employee =  result.data;
           self.employee.birth_date = new Date( self.employee.birth_date);
           self.employee.hire_date = new Date( self.employee.hire_date);
           console.log(self.employee.birth_date);
        })

        self.saveEmployee = saveEmployee;

        function saveEmployee(){
            console.log("save employee ...");
            console.log(self.employee.first_name);
            console.log(self.employee.last_name);
            console.log(self.employee.hire_date);
            console.log(self.employee.birth_date);
            console.log(self.employee.gender);
            EMSAppAPI.updateEmployee(self.employee).then((result)=>{
                console.log(result);
                $rootScope.$broadcast('refreshEmployeeList');
             }).catch((error)=>{
                console.log(error);
             })
            $uibModalInstance.close(self.run);
        }

    }






 function addEmployee(size, parentSelector){
            console.log("post add employee  ....");
            var items = [];
            var parentElem = parentSelector ? 
                angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
            var modalInstance = $uibModal.open({
                animation: self.animationsEnabled,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: './app/addEmployee.html',
                controller: 'AddEmployeeCtrl',
                controllerAs: 'ctrl',
                size: size,
                appendTo: parentElem,
                resolve: {
                    items: function () {
                        return items;
                    }
                }
            }).result.catch(function (resp) {
                if (['cancel', 'backdrop click', 'escape key press'].indexOf(resp) === -1)      throw resp;
            });
            
        }

function editEmployee(emp_no, size, parentSelector){
            console.log("Edit Employee...");
            console.log("emp_no > " + emp_no);
            
            var parentElem = parentSelector ? 
            angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
            var modalInstance = $uibModal.open({
                animation: self.animationsEnabled,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: './app/editEmployee.html',
                controller: 'EditEmployeeCtrl',
                controllerAs: 'ctrl',
                size: size,
                appendTo: parentElem,
                resolve: {
                    items: function () {
                        return emp_no;
                    }
                }
            }).result.catch(function (resp) {
                if (['cancel', 'backdrop click', 'escape key press'].indexOf(resp) === -1)      throw resp;
            });
        }


 function deleteEmployee(emp_no, size, parentSelector){
            console.log("delete Employee...");
            console.log("emp_no > " + emp_no);
            
            var parentElem = parentSelector ? 
            angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
            var modalInstance = $uibModal.open({
                animation: self.animationsEnabled,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: './app/deleteEmployee.html',
                controller: 'DeleteEmployeeCtrl',
                controllerAs: 'ctrl',
                size: size,
                appendTo: parentElem,
                resolve: {
                    items: function () {
                        return emp_no;
                    }
                }
            }).result.catch(function (resp) {
                if (['cancel', 'backdrop click', 'escape key press'].indexOf(resp) === -1)      throw resp;
            });
        }


*/